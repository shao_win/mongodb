package com.shaowin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shaowin.domain.Product;
import redis.clients.jedis.Jedis;

import java.io.IOException;

/**
 * Created by chenshaowen on 2017/2/4.
 */
public class JedisTest {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper=new ObjectMapper();
        Jedis jedis = new Jedis("101.200.169.59");

        //存储对象
        Product product=new Product("商品1",100);
        jedis.set("product:1", mapper.writeValueAsString(product));
        String value = jedis.get("product:1");
        Product product1=mapper.readValue(value,Product.class);
        System.out.println("product:1="+value);

    }
}
